import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    v_menu:true,
    current_theme_index:0,
    menu:[
      {
        text:'儀表板',
        class:'fas fa-tachometer-alt',
        notifications:1,
        path:'/'
      },
      {
        text:'公告',
        class:'far fa-bell',
        path:'/News'
      },
      {
        text:'簽核',
        class:'fas fa-list-ol',
        path:'/Form'
      },
      {
        text:'行事曆',
        class:'far fa-calendar-alt',
        notifications:999,
        path:'/News'
      },
      {
        text:'任務管理',
        class:'fas fa-address-book',
         notifications:20,
        path:'/News'
      },
      {
        text:'權限',
        class:'fas fa-unlock-alt',
        path:'/News'
      },
      {
        text:'個人化',
        class:'fas fa-drafting-compass',
        path:'/News'
      },
      {
        text:'設定',
        class:'fas fa-cog',
        path:'/News'
      },
    ],
    themes:[
      {
        name:'靜謐',
        primary_color:'#247ba0',
        second_color:'#70c1b3',
        normal_color:'#b2dbbf'
      },
      {
        name:'德式作風',
        primary_color:'#50514f',
        second_color:'#f25f5c',
        normal_color:'#ffe066'
      },
      {
        name:'郊遊',
        primary_color:'#5bc0eb',
        second_color:'#fde74c',
        normal_color:'#9bc53d'
      },
      {
        name:'紫陽花',
        primary_color:'#f2d7ee',
        second_color:'#f25f5c',
        normal_color:'#ffe066',
        font_color:'#563c04'
      },
    ],
    default_value:{
      font_color:'#ebeef5'
    }
  },
  mutations: {
    switch_theme (state, index) {
      state.current_theme_index = index;
    },
    switch_menu(state){
      state.v_menu = !state.v_menu;
    }
  },
  actions: {

  },
  getters: {
    current_theme: state => {
      let currentTheme = state.themes[state.current_theme_index];
      if(!currentTheme.font_color){
        currentTheme.font_color = state.default_value.font_color;
      }
      return currentTheme;
    }
  }
})
